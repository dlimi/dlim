<details>
 <summary>
  <h1>:book: Philosophy and ideas :pen_ballpoint:</h1>
 </summary>

 ### Abstract
 Enhancing the players capabilities instead of stifling them.

 Push the player into the "fun zone", by enforcing aggression and intelligent management of resources.
 Which may be achieved by punishing the player for defaulting to boring solutions.

 Such as increasing survivor dodge speed and distance, and then increasing its cooldown. Then in tandem reducing the night hunters GP distance, and decreasing cooldown, or increasing cooldown but making it more powerful.

 Or instead of nerfing survivor's capabilities as more players join, buffing the hunters capabilities, allowing them more independent freedom.
 This can cover for 1v1 scenarios with 3v1 balancing. As well as improving the pace of the game. It entices the players to be more aggressive.

 By making both sides an overwhelming force, you make it hard for them to play poorly. As they may get punished easier.
 It is less costly to make mistakes as hunter, when your opponent has a harder time managing flares for example.
 #

</details>
