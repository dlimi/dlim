# List of removed files
List components and their files and directories deleted from the vanilla game.


#### `ChromEd`
Removed due to redundancy, it does not serve a functional purpose.

+ `~/DW/Data0/data/.chromed_edit_decal_meshes.scr`

+ `~/DW/Data0/data/.chromed_edit_decal_templates.scr`

+ `~/DW/Data0/data/.chromed_fsmon_ignored_files.scr`

+ `~/DW/Data0/data/.chromed_navmesh_tools.def`

+ `~/DW/Data0/data/.chromed_navmesh_tools.scr`

+ `~/DW/Data0/data/.chromed_remove_at_build.scr`

+ `~/DW/Data0/data/.chromed_views.xml`

+ `~/DW/Data0/data/.chromedbuildfilesgenerator.scr`

+ `~/DW/Data0/data/.chromedcheckoutmap.scr`

+ `~/DW/Data0/data/.chromedfieldstips.scr`

+ `~/DW/Data0/data/.chromedicons.scr`

+ `~/DW/Data0/data/.chromediconsgame.scr`

+ `~/DW/Data0/data/.chromediconslayer.scr`

+ `~/DW/Data0/data/.chromediconsmapping.scr`

+ `~/DW/Data0/data/.chromedikoptions.scr`

+ `~/DW/Data0/data/.chromedinit.scr`

+ `~/DW/Data0/data/.chromednewfeatures.scr`

+ `~/DW/Data0/data/.chromedobjectfilters.scr`

+ `~/DW/Data0/data/.chromedobjectfiltersgame.scr`

+ `~/DW/Data0/data/.chromedsections.scr`

+ `~/DW/Data0/data/.chromedshortcutscopes.scr`

+ `~/DW/Data0/data/.chromedskineditor.scr`

+ `~/DW/Data0/data/.chromedstartgame.scr`

+ `~/DW/Data0/data/.chromedtempresources.scr`

+ `~/DW/Data0/data/.chromedterrainpaintpresets.scr`

+ `~/DW/Data0/data/.chromedtexts.scr`

+ `~/DW/Data0/data/.chromedviewpresets.scr`

+ `~/DW/Data0/data/.chromeedsetup.scr`

#
