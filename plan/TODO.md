### Debloat

- [ ] Remove non-PC player client components.
	- [x] Remove `ChromEd` components. (*Mostly complete*)
	- [ ] Remove PlayStation components. (*Work in progress*)
	- [ ] Remove XBOX components. (*Work in progress*)
	- [ ] Remove WiiU components. (*Work in progress*)
	- [ ] Remove 3DS components. (*Work in progress*)
	- [ ] Remove Android components. (*Work in progress*)
	- [ ] Remove iOS components. (*Work in progress*)
	- [ ] Remove native Linux components, to prioritize Wine instead. (*Work in progress*)
	- [ ] Remove legacy `Direct3D` components. (*Work in progress*)
	- [ ] Remove legacy `OpenGL` components. (*Work in progress*)

- [ ] Remove vestigial PC code.
	- [ ] Remove duplicate variables. (*Work in progress*)
	- [ ] Fix [broken import implementations](https://gitlab.com/dlimi/dlim/-/issues/2) among other script fixes.
	- [ ] Reduce redundancy in header/implementation chains, by moving headers to implementation files if necessary. (*Work in progress*)

- [ ] Correct file indentation and line-endings for size and internal markers.
	- [ ] Implement `LF` line endings. (*Work in progress*)
	- [ ] Implement TABS and one-space indentation for parser speed, and code-base size optimization. (*Work in progress*)

#

### Optimize

- [ ] Rework renderer (***GPU***) components globally.
	- [ ] View Distance tweaks
	- [ ] `LOD` tweaks
	- [ ] `PCSS` tweaks
	- [ ] Shadow tweaks (*Work in progress*)
	- [ ] `SSAO`, `HBAO` tweaks
	- [ ] PostProcessing tweaks
	- [x] Default settings tweaks.


- [ ] Rework general (***GPU***) components per-map implementation.
	- [ ] View Distance tweaks
	- [ ] `LOD` tweaks
	- [ ] Shadow tweaks

- [ ] Rework general (***CPU***) components globally.
	- [ ] Rework AI behavior and density.
	- [ ] Rework engine script logic.

#

### Stabilize

- [ ] Fix destabilizing bugs

- [ ] Improve frametime, framepacing, and input-response-time.

#
